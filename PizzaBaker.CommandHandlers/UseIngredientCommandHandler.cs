﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cqrs.Commands;
using Cqrs.Domain;
using PizzaBaker.AggregateRoots;
using PizzaBaker.Commands;

namespace PizzaBaker.CommandHandlers
{
	public class UseIngredientCommandHandler : ICommandHandler<string, UseIngredientCommand>
	{
		protected IUnitOfWork<string> UnitOfWork { get; private set; }

		public UseIngredientCommandHandler(IUnitOfWork<string> unitOfWork)
		{
			UnitOfWork = unitOfWork;
		}

		public void Handle(UseIngredientCommand command)
		{
			Inventory inventoryAggregateRoot = UnitOfWork.Get<Inventory>(command.Rsn);
			//UnitOfWork.Add(inventoryAggregateRoot);
			inventoryAggregateRoot.UseIngredient(command.Name, command.Amount, command.Unit);
			UnitOfWork.Commit();
		}
	}
}
