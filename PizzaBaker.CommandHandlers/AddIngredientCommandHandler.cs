﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cqrs.Commands;
using Cqrs.Domain;
using PizzaBaker.AggregateRoots;
using PizzaBaker.Commands;

namespace PizzaBaker.CommandHandlers
{
	public class AddIngredientCommandHandler : ICommandHandler<string, AddIngredientCommand>
	{
		protected IUnitOfWork<string> UnitOfWork { get; private set; }

		public AddIngredientCommandHandler(IUnitOfWork<string> unitOfWork)
		{
			UnitOfWork = unitOfWork;
		}

		public void Handle(AddIngredientCommand command)
		{
			Inventory inventoryAggregateRoot;
			try
			{
				inventoryAggregateRoot = UnitOfWork.Get<Inventory>(command.Rsn);
			}
			catch (Exception ex)
			{
				inventoryAggregateRoot = new Inventory(command.Rsn);
				UnitOfWork.Add(inventoryAggregateRoot);
			}

			//(_aggregateRepository as AggregateRepository<string>).LoadAggregateHistory(inventoryAggregateRoot);
			//var inventoryAggregateRoot = new Inventory();
			
			inventoryAggregateRoot.AddIngredient(command.Name, command.Amount, command.Unit);
			UnitOfWork.Commit();
		}
	}
}
