﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cdmdotnet.Logging;
using Cqrs.Configuration;
using Cqrs.Domain;
using PizzaBaker.Events.Inventory;
using Db = PizzaBaker.Projections.Inventory;

namespace PizzaBaker.AggregateRoots
{
	public class Inventory : AggregateRoot<string>
	{
		public Db.Inventory CurrentInventory { get; set; } = new Db.Inventory();
		public Guid Rsn
		{
			get { return Id; }
			private set { Id = value; }
		}

		public Inventory(IDependencyResolver dependencyResolver, ILogger logger, Guid? rsn)
		{
			Rsn = rsn ?? Guid.NewGuid();
		}

		public Inventory(Guid rsn)
		{
			Rsn = rsn;
		}

		public void AddIngredient(string name, int amount, string unit)
		{
			IngredientAddedEvent ingredientAddedEvent = new IngredientAddedEvent(Rsn, name, amount, unit);
			ApplyChange(ingredientAddedEvent);
		}

		public void UseIngredient(string name, int amount, string unit)
		{
			IngredientUsedEvent ingredientUsedEvent = new IngredientUsedEvent(Rsn, name, amount, unit);
			ApplyChange(ingredientUsedEvent);
		}

		public void Apply(IngredientAddedEvent @event)
		{
			Db.Ingredient ingredient;
			if (!CurrentInventory.Ingredients.Any(i => i.Name == @event.Name))
			{
				ingredient = new Db.Ingredient
				{
					Name = @event.Name,
					Unit = @event.Unit
				};
				CurrentInventory.Ingredients.Add(ingredient);
			}
			else
			{
				ingredient = CurrentInventory.Ingredients.First(i => i.Name == @event.Name);
			}

			ingredient.Amount += @event.Amount;
		}

		public void Apply(IngredientUsedEvent @event)
		{

			if (!CurrentInventory.Ingredients.Any(i => i.Name == @event.Name))
			{
				throw new ArgumentOutOfRangeException($"Could not find ingredient {@event.Name} in the inventory");
			}

			Db.Ingredient ingredient = CurrentInventory.Ingredients.First(i => i.Name == @event.Name);
			ingredient.Amount -= @event.Amount;
		}
	}
}
