﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cqrs.Events;

namespace PizzaBaker.Events.Inventory
{
	public class IngredientUsedEvent : IEvent<string>
	{
		public string Name { get; set; }
		public int Amount { get; set; }
		public string Unit { get; set; }

		public IngredientUsedEvent(Guid rsn, string name, int amount, string unit)
		{
			Rsn = rsn;
			Name = name;
			Amount = amount;
			Unit = unit;
		}

		#region inherited
		public Guid Rsn { get; set; }
		public Guid Id { get; set; }
		public int Version { get; set; }
		public DateTimeOffset TimeStamp { get; set; }
		public string AuthenticationToken { get; set; }
		public Guid CorrelationId { get; set; }
		public string OriginatingFramework { get; set; }
		public IEnumerable<string> Frameworks { get; set; }
#endregion
	}
}
