﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Cqrs.Commands;
using Cqrs.Configuration;
using Cqrs.Events;
using Cqrs.EventStore;
using PizzaBaker.CommandHandlers;
using PizzaBaker.Commands;
using PizzaBaker.Projections;
using PizzaBaker.Projections.Inventory;

namespace PizzaBaker
{
	class Program
	{
		static void Main(string[] args)
		{
			ConfigureDependencies();
			
			using (var runtime = new EventStoreRuntime<string>())
			{
				runtime.RegisterHandler(typeof(AddIngredientCommandHandler));
				//runtime.RegisterHandler(typeof(UseIngredientCommandHandler));
				runtime.RegisterHandler(typeof(ConsoleNotifier));
				IEventPublisher<string> eventPublisher = DependencyResolver.Current.Resolve<IEventPublisher<string>>();

				string command = "h";
				while (HandleUserEntry(command))
				{
					command = Console.ReadLine();
				}
			}
		}
		private static bool HandleUserEntry(string text)
		{
			switch (text.ToLowerInvariant())
			{
				case "h":
					Console.WriteLine("ai");
					Console.WriteLine("\tWill allow you to add an ingredient.");
					Console.WriteLine("ui");
					Console.WriteLine("\tWill allow you to use an ingredient.");
					Console.WriteLine("si");
					Console.WriteLine("\tWill allow you to show the inventory.");
					Console.WriteLine("q");
					Console.WriteLine("\tWill exit the running program.");
					Console.WriteLine("h");
					Console.WriteLine("\tWill display this help menu.");
					break;
				case "ai":
					Console.WriteLine("Enter the ingredient name.");
					string name = Console.ReadLine();
					Console.WriteLine("Enter the ingredient amount.");
					int amount = int.Parse(Console.ReadLine());
					Console.WriteLine("Enter the ingredient unit.");
					string unit = Console.ReadLine();

					var commandPublisher = DependencyResolver.Current.Resolve<ICommandPublisher<string>>();
					commandPublisher.Publish(new AddIngredientCommand(name, amount, unit, Guid.Parse("d330ca54-ff99-4325-9773-2a396476099a")));

					break;
				case "ui":
					Console.WriteLine("Enter the ingredient name.");
					name = Console.ReadLine();
					Console.WriteLine("Enter the ingredient amount.");
					amount = int.Parse(Console.ReadLine());
					Console.WriteLine("Enter the ingredient unit.");
					unit = Console.ReadLine();

					commandPublisher = DependencyResolver.Current.Resolve<ICommandPublisher<string>>();
					commandPublisher.Publish(new UseIngredientCommand(name, amount, unit, Guid.Parse("d330ca54-ff99-4325-9773-2a396476099a")));

					break;
				case "si":
					var inventory = InventoryDb.Inventory;

					foreach (var ingredient in inventory.Ingredients)
					{
						Console.WriteLine($"Ingredient: {ingredient.Name}");
						Console.WriteLine($"Amount: {ingredient.Amount} {ingredient.Unit}");
						Console.WriteLine($"Times adjusted: {ingredient.NumberOfChanges}");
						Console.WriteLine($"--");
					}
					Console.WriteLine($"----");
					break;
				case "q":
					return false;
			}
			return true;
		}

		private static IContainer ConfigureDependencies()
		{
			var builder = new ContainerBuilder();

			builder.Register();
			var container = builder.Build();
			AutofacDependencyResolver.Container = container;
			//AutofacDependencyResolver.Current = new AutofacDependencyResolver();
			return container;
		}
	}
}
