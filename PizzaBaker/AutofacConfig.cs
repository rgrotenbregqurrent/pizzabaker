﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using cdmdotnet.Logging;
using cdmdotnet.Logging.Configuration;
using cdmdotnet.StateManagement;
using cdmdotnet.StateManagement.Threaded;
using Cqrs.Authentication;
using Cqrs.Bus;
using Cqrs.Commands;
using Cqrs.Configuration;
using Cqrs.Domain;
using Cqrs.Domain.Factories;
using Cqrs.Events;
using Cqrs.EventStore;
using Cqrs.EventStore.Bus;
using Cqrs.Repositories.Queries;
using EventStore.ClientAPI;
using ILogger = cdmdotnet.Logging.ILogger;

namespace PizzaBaker
{
	public static class AutofacConfig
	{

		public static void Register(this ContainerBuilder builder)
		{
			builder.RegisterType<ThreadedContextItemCollectionFactory>().AsImplementedInterfaces().AsSelf().SingleInstance();
			builder.RegisterType<CorrelationIdHelper>().AsImplementedInterfaces().AsSelf().SingleInstance();
			builder.RegisterType<ConfigurationManager>().AsImplementedInterfaces().AsSelf().SingleInstance();
			builder.RegisterType<LoggerSettings>().AsImplementedInterfaces().AsSelf().SingleInstance();
			builder.RegisterType<TraceLogger>().AsImplementedInterfaces().InstancePerLifetimeScope();
			builder.RegisterType<Cqrs.EventStore.EventFactory<string>>().AsImplementedInterfaces().InstancePerLifetimeScope();
			builder.RegisterType<EventStoreConnectionHelper<string>>().AsImplementedInterfaces().InstancePerLifetimeScope();

			var configurationManager = new ConfigurationManager();
			var eventStoreFactory = new Cqrs.EventStore.EventFactory<string>();
			var eventStoreConnectionHelper = new EventStoreConnectionHelper<string>(eventStoreFactory, configurationManager);
			var eventStoreBasedLastEventProcessedStore = new EventStoreBasedLastEventProcessedStore(eventStoreConnectionHelper.GetEventStoreConnection());
			builder.Register(context => eventStoreBasedLastEventProcessedStore)
				.As<IStoreLastEventProcessed>()
				.SingleInstance();

			var eventStore = new Cqrs.EventStore.EventStore<string>(eventStoreFactory, eventStoreFactory, eventStoreConnectionHelper);
			builder.Register(context => eventStore)
				.As<IEventStore<string>>()
				.SingleInstance();

			builder.Register(context => new EventStoreEventPublisher<string>(eventStoreConnectionHelper, eventStoreBasedLastEventProcessedStore))
				.As<IEventPublisher<string>>()
				.SingleInstance();

			builder.RegisterType<DefaultAuthenticationTokenHelper>().AsImplementedInterfaces().InstancePerLifetimeScope();
			builder.RegisterType<BusHelper>().AsImplementedInterfaces().InstancePerLifetimeScope();
			builder.Register(context => AutofacDependencyResolver.Current).As<IDependencyResolver>()
				.SingleInstance();
			builder.RegisterType<InProcessBus<string>>().AsImplementedInterfaces().InstancePerLifetimeScope();

			builder.RegisterType<AggregateFactory>().AsImplementedInterfaces().InstancePerLifetimeScope();
			builder.RegisterType<AggregateRepository<string>>().AsImplementedInterfaces().InstancePerLifetimeScope();

			builder.RegisterType<UnitOfWork<string>>().AsImplementedInterfaces().InstancePerLifetimeScope();
			builder.RegisterType<AuthenticationTokenHelper<string>>().AsImplementedInterfaces().InstancePerLifetimeScope();
			builder.RegisterType<QueryFactory>().AsImplementedInterfaces().InstancePerLifetimeScope();

			foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				if (assembly.FullName.Contains("PizzaBaker"))
				{

					builder.RegisterAssemblyTypes(assembly)
						.Where(type => type.IsAssignableTo<IEventHandler>())
						.As<IEventHandler>()
						.AsSelf()
						.InstancePerDependency();
					builder.RegisterAssemblyTypes(assembly)
						.Where(type => type.IsAssignableTo<ICommandHandle>())
						.As<ICommandHandle>()
						.AsSelf()
						.InstancePerDependency();
				}
			}
		}
	}

	public class AutofacDependencyResolver : DependencyResolver, IDisposable
	{
		public static IContainer Container { get; set; }

		static AutofacDependencyResolver()
		{
			Current = new AutofacDependencyResolver();
		}
		public override T Resolve<T>()
		{
			return Container.Resolve<T>();
		}

		public override object Resolve(Type type)
		{
			return Container.Resolve(type);
		}

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~AutofacDependencyResolver() {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}


		#endregion

	}
}
