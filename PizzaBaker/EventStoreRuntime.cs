﻿using Cqrs.Configuration;
using Cqrs.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using cdmdotnet.Logging;
using cdmdotnet.Logging.Configuration;
using cdmdotnet.StateManagement;
using cdmdotnet.StateManagement.Threaded;
using Cqrs.Authentication;
using Cqrs.Bus;
using Cqrs.Commands;
using Cqrs.Domain;
using Cqrs.Domain.Factories;
using Cqrs.EventStore;
using Cqrs.EventStore.Bus;
using Cqrs.Repositories.Queries;


namespace PizzaBaker
{
	public class EventStoreRuntime<TAuthenticationToken> : IDisposable
	{
		/// <summary>
		/// The <see cref="Func{TResult}"/> used to create the <see cref="IEventStore{TAuthenticationToken}"/>
		/// </summary>
		protected static Func<IDependencyResolver, IEventStore<TAuthenticationToken>> EventStoreCreator { get; set; }

		/// <summary>
		/// A custom dependency resolver.
		/// </summary>
		public static Func<IDependencyResolver, Type, object> CustomResolver { get; set; }

		/// <summary>
		/// Instaiance a new instance of the <see cref="SampleRuntime{TAuthenticationToken,TCommandHanderOrEventHandler}"/>
		/// </summary>
		public EventStoreRuntime()
		{
			SetEventStoreCreator();
		}

		/// <summary>
		/// Sets the <see cref="EventStoreCreator"/> to use <see cref="InProcessEventStore{TAuthenticationToken}"/>
		/// </summary>
		protected virtual void SetEventStoreCreator()
		{
			//EventStoreCreator = dependencyResolver => new InProcessEventStore<TAuthenticationToken>();
			EventStoreCreator = dependencyResolver => new Cqrs.EventStore.EventStore<TAuthenticationToken>(
				dependencyResolver.Resolve<Cqrs.EventStore.IEventBuilder<TAuthenticationToken>>(),
				dependencyResolver.Resolve<Cqrs.EventStore.IEventDeserialiser<TAuthenticationToken>>(),
				dependencyResolver.Resolve<Cqrs.EventStore.IEventStoreConnectionHelper>()
			);
		}

		/// <summary>
		/// Registers the all <see cref="IEventHandler"/> and <see cref="ICommandHandle"/>.
		/// </summary>
		public virtual void RegisterHandler(Type type)
		{
			new BusRegistrar(DependencyResolver.Current)
				.Register(type);
		}

		/// <summary>
		/// Prints out the statistics of this run such as the number of event raised to the <see cref="Console"/>.
		/// </summary>
		public virtual void PrintStatsticsToConsole()
		{
			//var inProcStore = DependencyResolver.Current.Resolve<IEventStore<TAuthenticationToken>>() as InProcessEventStore<TAuthenticationToken>;
			//if (inProcStore != null)
			//{
			//	var inMemoryDb = typeof(InProcessEventStore<TAuthenticationToken>).GetProperty("InMemoryDb", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(inProcStore, null) as IDictionary<Guid, IList<IEvent<TAuthenticationToken>>>;
			//	Console.WriteLine("{0:N0} event{1} {2} raised.", inMemoryDb.Count, inMemoryDb.Count == 1
			//			? null
			//			: "s", inMemoryDb.Count == 1
			//			? "was"
			//			: "were");
			//}
		}

		#region Implementation of IDisposable

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{

		}

		#endregion
	}
}
