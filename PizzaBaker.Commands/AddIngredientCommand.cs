﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cqrs.Commands;

namespace PizzaBaker.Commands
{
	public class AddIngredientCommand : ICommandWithIdentity<string>
	{
		public string Name { get; set; }
		public int Amount { get; set; }
		public string Unit { get; set; }

		public AddIngredientCommand(string name, int amount, string unit, Guid? rsn = null)
		{
			Id = Guid.NewGuid();
			Rsn = rsn ?? Guid.NewGuid();
			Name = name;
			Amount = amount;
			Unit = unit;
		}

#region inherited members
		public Guid CorrelationId { get; set; }
		public string OriginatingFramework { get; set; }
		public IEnumerable<string> Frameworks { get; set; }
		public string AuthenticationToken { get; set; }
		public Guid Id { get; set; }
		public int ExpectedVersion { get; set; }
		public Guid Rsn { get; set; }
#endregion
	}
}
