﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cqrs.Events;
using PizzaBaker.Events.Inventory;

namespace PizzaBaker.Projections
{
	public class ConsoleNotifier : IEventHandler<string, IngredientAddedEvent>
	{
		public void Handle(IngredientAddedEvent message)
		{
			WriteMessage(nameof(IngredientAddedEvent), $"Ingredient: {message.Name}, amount: {message.Amount}, unit: {message.Unit}");
		}

		private void WriteMessage(string eventName, string message)
		{
			Console.WriteLine($"Event recieved: {eventName}");
			Console.WriteLine($"Sane data: {message}");
			Console.WriteLine("----");
		}
	}
}
