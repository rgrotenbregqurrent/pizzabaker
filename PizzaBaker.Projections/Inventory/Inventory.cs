﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaBaker.Projections.Inventory
{
	public class Inventory
	{
		public Inventory()
		{
			Ingredients = new List<Ingredient>();
		}
		public List<Ingredient> Ingredients { get; set; }
	}
}
