﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaBaker.Projections.Inventory
{
	public class Ingredient
	{
		public string Name { get; set; }
		public int Amount { get; set; }
		public string Unit { get; set; }
		public int NumberOfChanges { get; set; }
	}
}
