﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cqrs.Events;
using PizzaBaker.Events.Inventory;

namespace PizzaBaker.Projections.Inventory
{
	public class UpdateIngredientEventHandler : IEventHandler<string, IngredientAddedEvent>
	{
		
		//public UpdateIngredientEventHandler(IEventStore<string> store)
		//{
		//	foreach (var historicalEvent in store.Get<string>(Guid.Parse("243443dc-1b90-4c32-b39e-62248796a777")))
		//	{
		//		if(historicalEvent is IngredientAddedEvent)
		//			Handle(historicalEvent as IngredientAddedEvent);
		//	}	
		//}

		public void Handle(IngredientAddedEvent message)
		{
			Ingredient ingredient;
			if (!InventoryDb.Inventory.Ingredients.Any(i =>
				i.Name == message.Name
			//ignore unit for now
			))
			{
				ingredient = new Ingredient
				{
					Name = message.Name,
					Unit = message.Unit,
					Amount = 0,
					NumberOfChanges = 0
				};
				InventoryDb.Inventory.Ingredients.Add(ingredient);
			}
			else
			{
				ingredient = InventoryDb.Inventory.Ingredients.First(i => i.Name == message.Name);
			}

			ingredient.Amount += message.Amount;
			ingredient.NumberOfChanges++;

		}
	}
}
