﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaBaker.Projections.Inventory
{
	public static class InventoryDb
	{
		static InventoryDb()
		{
			Inventory = new Inventory();
		}
		public static Inventory Inventory { get; set; }
	}
}
